package com.example.ccrmobile.ui

import android.view.View
import com.example.ccrmobile.data.Author

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClicked(view: View, author: Author)
}